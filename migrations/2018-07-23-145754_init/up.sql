-- Your SQL goes here
CREATE TABLE readings (
    id          BIGSERIAL NOT NULL,
    date_time   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    value       DOUBLE PRECISION NOT NULL,
    published   BOOLEAN NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO readings (value, published) VALUES (0.0, false);
INSERT INTO readings (value, published) VALUES (1.0, true);
