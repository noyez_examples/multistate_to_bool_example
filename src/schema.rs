table! {
    readings (id) {
        id -> Int8,
        date_time -> Timestamp,
        value -> Float8,
        published -> Bool,
    }
}
