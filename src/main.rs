#[macro_use] extern crate diesel;
extern crate dotenv;
extern crate chrono;

#[macro_use] extern crate serde_derive;
extern crate serde;
extern crate serde_json;

use diesel::prelude::*;
use diesel::sql_types::Bool;
use diesel::pg::Pg;
use diesel::serialize::{ToSql, Output};
use diesel::deserialize::{FromSql};

use chrono::prelude::*;
use diesel::pg::PgConnection;
use dotenv::dotenv;
use std::env;


pub mod schema;
use schema::readings;

#[derive(Debug, Clone, PartialEq, AsExpression, FromSqlRow, Serialize)]
#[sql_type = "Bool"]
pub enum PublishState
{
    Published,
    Unpublished,
    Pending,
}

impl ToSql<Bool, Pg> for PublishState {
    fn to_sql<W: std::io::Write>(&self, out: &mut Output<W, Pg>) -> ::diesel::serialize::Result {
        match self {
            &PublishState::Published =>  { ToSql::<Bool, Pg>::to_sql(&true, out) },
            &PublishState::Unpublished | &PublishState::Pending =>  { ToSql::<Bool, Pg>::to_sql(&false, out) }
        }
    }
}

impl FromSql<diesel::sql_types::Bool, Pg> for PublishState {
    fn from_sql(bytes: Option<&[u8]>) -> diesel::deserialize::Result<Self> {
        println!("Reading from sql: bytes:{:?}", bytes);
        match bytes {
            Some(bytes) => if bytes[0] != 0 { Ok(PublishState::Published) } else { Ok(PublishState::Unpublished) },
            None => Ok(PublishState::Unpublished),
        }
    }
}

struct PublishStateVisitor;
use serde::de::{self, Visitor};
use serde::de::Deserialize;
use serde::de::Deserializer;

impl<'de> Visitor<'de> for PublishStateVisitor {
    type Value = PublishState;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("a boolean or string of \"Published\", \"Unpublished\", \"Pending\".")
    }

    fn visit_bool<E>(self, value: bool) -> Result<PublishState, E>
    where
        E: de::Error,
    {
        match value {
            true => Ok(PublishState::Published),
            false => Ok(PublishState::Unpublished),
        }
    }

    fn visit_str<E>(self, value: &str) -> Result<PublishState, E>
    where
        E: de::Error,
    {
        match value {
            "Published" => Ok(PublishState::Published),
            "Unpublished" | "Pending" => Ok(PublishState::Unpublished),
            _s => Err(E::custom(format!("Unknown string value: {}", _s))),
        }
    }
}

impl<'de> Deserialize<'de> for PublishState {
    fn deserialize<D>(deserializer: D) -> Result<PublishState, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_any(PublishStateVisitor)
    }
}

#[derive(PartialEq, Debug, Clone, Queryable, Identifiable, Insertable, AsChangeset, Associations, Serialize, Deserialize)]
#[table_name="readings"]
pub struct ReadingDbEntity {
    pub id: i64,
    pub date_time: NaiveDateTime,
    pub value: f64,
    pub published: PublishState,
}

#[derive(PartialEq, Debug, Clone, Insertable, )]
#[table_name="readings"]
pub struct NewReadingDbEntity {
    pub value: f64,
    pub published: PublishState,
}


pub fn establish_connection() -> PgConnection {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url))
}

fn main() -> std::io::Result<()>
{
    let diesel_conn = establish_connection();
    let new_example_readings = NewReadingDbEntity { value: 0.1, published: PublishState::Pending };
    let r : Vec<ReadingDbEntity> = diesel::insert_into(::schema::readings::table).values(&new_example_readings).get_results(&diesel_conn).expect("Can't save new data");
    println!("r: {:?}", r);

    let serialized = serde_json::to_string_pretty(&r[0])?;
    println!("serialized: {}", serialized);


    let serialized_data_with_string = r#"{
        "id": 4,
        "date_time": "2018-08-01T12:53:27.977006",
        "value": 0.1,
        "published": "Unpublished"
    }"#;

    let reading_db_entity : ReadingDbEntity = serde_json::from_str(serialized_data_with_string)?;
    println!("using string: db_entity: {:#?}", reading_db_entity);

    let serialized_data_with_bool = r#"{
        "id": 4,
        "date_time": "2018-08-01T12:53:27.977006",
        "value": 0.1,
        "published": false
    }"#;

    let reading_db_entity : ReadingDbEntity = serde_json::from_str(serialized_data_with_bool)?;
    println!("using bool: db_entity: {:#?}", reading_db_entity);



    Ok(())
}
